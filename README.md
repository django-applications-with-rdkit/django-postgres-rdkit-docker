# django-postgres-rdkit-docker

Docker environment for easily deploying django apps with RDKit

# How to use it

Clone this repository. The password of the database can be set in docker-postgres-rdkit/dbpassword (the database name and user is postgres). The file for the Django site is to be copied in docker-django-rdkit/code.